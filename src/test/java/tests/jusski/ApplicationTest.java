package tests.jusski;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.testng.Assert;
import org.testng.annotations.Test;

@Test(groups = "smoke")
public class ApplicationTest
{
    @Test
    void shouldPrintHelloWorld()
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        
        System.setOut(new PrintStream(bos));
        jusski.Application.main(null);
       
        Assert.assertTrue(bos.toString().contains("Hello world."));
    }

}
